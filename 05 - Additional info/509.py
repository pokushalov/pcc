from collections import Counter
c = Counter()
c.update(["Alex"])
print(c)
c.update(['Alex'])
print(c)
c.update(['Irma'])
print(c)
print(c.most_common(1))
c['Alex'] -= 1
print(c)
print(c.most_common(1))
