#!/usr/bin/env python
from collections import Counter

#################################################################################################################################

def main():
    file_name = 'bible.txt'
    with open(file_name, 'r') as file:
        data = file.read().lower().split()
        c = Counter()
        c.update(data)
        for word, cnt in c.most_common(10):
            print("{}: {}".format(word, cnt))
    return 0

#################################################################################################################################


if __name__ == '__main__':
    main()
