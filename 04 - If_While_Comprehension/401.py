#!/usr/bin/env python
a = 1
b = 2
str1 = 'Chase'
str2 = 'chase'

if a > b:
    print ("A > B")
else:
    print ("A <= B")

if str1 == str2:
    print ("Strings are equal")

if str1.lower() == str2.lower():
    print ("Lowered strings are equal")

# boolean
holiday = True
if holiday:
    print("Yay. Today is holiday")
holiday = False
if not holiday:
    print ("Not a holiday")
friday = True
if not holiday and friday:
    print ("Well, almost")
