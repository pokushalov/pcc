#!/usr/bin/env python
from pprint import pprint

oracle_dbs = {
    'oracle_prod': {
        'host': 'prod01',
                'port': '1571',
                'SID': 'prod_01', },
    'oracle_qa': {
        'host': 'qa12',
                'port': '1561',
                'SID': 'QA_001', },
}
for oracle_id, oracle_info in oracle_dbs.items():
    pprint('%s : %s' % (oracle_id, oracle_info))

print('*' * 80)
for ora_id, ora_info in oracle_dbs.items():
    print("\nOracle id: %s" % (ora_id))
    for db_info, db_param in ora_info.items():
        print("\t%s - %s" % (db_info, db_param))
