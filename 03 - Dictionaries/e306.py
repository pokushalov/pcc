#!/usr/bin/env python
from pprint import pprint


def ln():
    print('-'*80)


contact = {
    "first": "John",
    "last": "Doe",
    "age": 39,
    "sex": "M",
}
pprint(contact)
print("Just keys: %s" % (contact.keys()))
print("HAS_KEY: %s" % (contact.has_key("age")))
# SHOW THIS
# HAS_KEY was removed in python 3 !!!
print("IN (was HAS_KEY): %s" % ("age" in contact))
# SHOW THIS
# SHOW THIS
ln()
print('Printing KEYS')
for key in contact.keys():
    print("Key: %s" % (key))
ln()
print('Printing VALUES')
for value in contact.values():
    print("Value: %s" % (value))
ln()
print('Printing KEYS, VALUES')
for key, value in contact.items():
    print("Key: %s, Valule: %s" % (key, value))
ln()
for tpl in contact.items():
    print(tpl)
    print('Tuple: %s - %s ' % (tpl[0], tpl[1]))
