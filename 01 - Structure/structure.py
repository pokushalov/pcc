# Demo for __main__ and
###############################################################################


def hello_world(name):
    print("Hello world from %s" % (name))


def main():
    print("Def MAIN executed")
    hello_world("Alex_structure")


if __name__ == "__main__":
    main()
