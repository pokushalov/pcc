# reverse string
a = "ABCDEF"
print (a[::-1])

print ("Iterating over reversed")
# iterating over reversed string
for char in reversed(a):
    print(char)

num = 123456789
print (reversed(num))

print (int(str(num)[::-1]))
