#!/usr/bin/env python
from pprint import pprint
lst = [1, 2, 3, 4]
dct = {1: 'first', 2: 'second'}
pprint("List: {}".format(lst))
pprint("Dictionary: {}".format(dct))
print ("*" * 20)
x_yes = 1
x_no = 9
z_yes = 2
yes_str = 'second'

for val in [x_yes, x_no]:
    print ("Running check for list for %s" % (val))
    if val in lst:
        print ("\tYes {} in this list {}".format(x_yes, lst))
    else:
        print ("\tNop, no {} in {}".format(x_yes, lst))

# check dict now
print ("Checking dictionary key")
if z_yes in dct:
    print ("Yes value {} is in dict".format(z_yes, dct))
if yes_str in dct.values():
    print ("Yes, '{}' is in values.".format(yes_str))
else:
    print ("No, '{}' is in values.".format(yes_str))
