#!/usr/bin/env python
lst = ['Dogs', 'Cats']
counter = [12, 2]
dct = {'Dogs': 12, 'Cats': 2}
print ("List")
for idx, item in enumerate(lst):
    print ("Index: {}, item: {}, number of animals: {}".format(idx, item, counter[idx]))

print ("*" * 20)
print ("Dictionary")
x = ()
for idx, (key, value) in enumerate(dct.items()):
    print ("Index: {}, item: {}, number of animals: {}".format(idx, key, dct[key]))

print ("*" * 20)
for x in dct.items():
    print (x)
    print (x[0])
    print (x[1])
