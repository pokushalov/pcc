# list to comma separated string
items = ['foo', 'bar', 'xyz']
print(','.join(items))
# numbers
numbers = [2, 3, 12, 13]
# MAP = Return an iterator that applies function to every item of iterable, yielding the results.
print(','.join(map(str,numbers)))

# 2 iterables
numbers2  = ['4', '2', '1']
numbers3 = ['A', 'B','8']
print(','.join(map(max, numbers2, numbers3)))

numbers2 = [7, 3, 0, 17]
print(list(map(max, numbers, numbers2)))

"""
MAP:
Return an iterator that applies function to every item of iterable, yielding the results. If additional iterable arguments are passed, function must take that many arguments and is applied to the items from all iterables in parallel. With multiple iterables, the iterator stops when the shortest iterable is exhausted. For cases where the function inputs are already arranged into argument tuples, see itertools.starmap.
"""
