def add(a, b):
    return(a + b)


def mult(a, b):
    return(a * b)


b = True
print((add if b else mult)(5, 7))
print ("-" * 20)
b = False
print((add if b else mult)(5, 7))
