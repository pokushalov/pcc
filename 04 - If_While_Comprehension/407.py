#!/usr/bin/env python
lst = ['Dogs', 'Cats']
counter = [12, 2]
longer_list = ['Dogs', 'Cats', 'Birds']

for kind, cnt in zip(lst, counter):
    print ("{}: {}".format(kind, cnt))
# now let's try longer list
print ("*" * 12)
for val in zip(longer_list, counter):
    print ("{}: {}".format(val[0], val[1]))
