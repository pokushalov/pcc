#!/usr/bin/env python

def myfunction(what):
    print("\tIn function, getting younger")
    what = 18
    print("\tI'm %d again" % (what))


age = 40
print("Real age is %d." % (age))
myfunction(age)
print("OK, getting back to %d again" % (age))
