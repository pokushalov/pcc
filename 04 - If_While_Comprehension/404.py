#!/usr/bin/env python
# increasing numbers
lst = [-10, -9, -5, -1, 0, 4, 9, 10]
# left pointer, right pointer
lp = 0
res = [0] * len(lst)
rp = len(lst) - 1
while lp <= rp:
    lval = abs(lst[lp])
    rval = abs(lst[rp])
    print (lval, rval)
    if lval > rval:
        res[rp - lp] = lval
        lp = lp + 1
    else:
        res[rp - lp] = rval
        rp -= 1
print (res)
