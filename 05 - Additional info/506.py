"""
Else gets called when for loop does not reach break statement
"""
a = [1, 2, 3, 4, 5]
for el in a:
    if el == 0:
        break
else:
    print ('Did not break out of for loop')

idx = 0
while idx < len(a):
    if a[idx] == 0:
        break
    idx += 1
else:
    print('Did not break out of while loop')
