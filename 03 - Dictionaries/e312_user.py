#!/usr/bin/env python
import e312_config
import fmt

fmt.ln()
print("All info in one:")
print(e312_config.oracle_dbs['oracle_prod'])
fmt.ln()
print("Port only:")
print(e312_config.oracle_dbs['oracle_prod']['port'])
fmt.ln()
print("Looping thru:")
for ora_key, ora_value in e312_config.oracle_dbs['oracle_prod'].items():
    print("\t%s - %s" % (ora_key, ora_value))
fmt.ln()
print("Getting all info:")
for db_k, db_val in e312_config.oracle_dbs.items():
    print("Getting info for DB %s:" % (db_k))
    for db_pref, db_pref_val in db_val.items():
        print("\t%s: %s" % (db_pref, db_pref_val))
fmt.ln()
