#!/usr/bin/env python
from pprint import pprint
contact = {
    "first": "John",
    "last": "Doe",
    "age": 39,
    "sex": "M",
}
print(contact.keys())

for key, value in contact.items():
    print("t=\tKey: %s, Valule: %s" % (key, value))

print("Removing age...")
del contact['age']
pprint(contact)
