from operator import itemgetter
"""
Sort dictionay by its values with the sorted() function and 'key' argument
"""
d = {'apple': 10, 'orange': 20, 'banana': 5, 'rotten tomato': 1}

print (d.items())
raw_input("By Key")
print(sorted(d.items(), key=lambda x: x[0]))
raw_input("By values")
print(sorted(d.items(), key=lambda x: x[1]))
raw_input("Reversed")
print(sorted(d.items(), key=lambda x: x[1], reverse=True))
raw_input("Item getter")
print(sorted(d.items(), key=itemgetter(1)))
print(sorted(d.items(), key=itemgetter(0)))

l = [1,2,3,4,5]
x = enumerate(l)

